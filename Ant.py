import pandas as pd
import math
import numpy as np

class AntSystem():
    data = None
    distance = None
    pheromone = None

    def __init__(self,path):
        self.data = pd.read_csv(path,delimiter=',')
        self.distance = self.__distance(self.data)
        self.pheromone = 0.00001 * np.ones([self.data.__len__(),self.data.__len__()])

    def __euclidean(self,x1, x2, y1, y2):
        return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

    def __distance(self,data):
        dist = np.zeros([data.__len__(), data.__len__()])
        for i in range(data.__len__()):
            for j in range(data.__len__()):
                if i != j:
                    dist[i, j] = dist[j, i] = self.__euclidean(data['X'][i], data['X'][j], data['Y'][i], data['Y'][j])
        return dist

    def len(self):
        return self.data.__len__();

    def test(self,list):
        cost = 0
        for i in range(len(list)-1):
            cost += self.distance[list[i+1]][list[i]]
        #     print('Distance {} to {} is {}'.format(list[i],list[i+1],self.distance[list[i+1]][list[i]]))
        # print('cost',cost)

class Fitness:
    fit = 0
    route = []

    def __init__(self,fit, route):
        self.fit = fit
        self.route = route