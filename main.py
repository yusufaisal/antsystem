import random as rand
from Ant import AntSystem,Fitness
import numpy as np
import operator
import matplotlib.pyplot as plt
from pprint import pprint
import pandas as pd
import time

start_time = time.time()

# constant parameter
alpha = 4
beta = 10

# 1. initiation
# X = AntSystem('Swarm096.tsp')
X = AntSystem('Swarm016.tsp') # change the file here

## parameter
NC = 1
NCmax = 3
rho = 0.6
Q = 0.5
numOfAnt = 20
best = []

# method
def deltaTau(oldTau,distance):
    newTau = Q / distance
    oldTau += newTau
    return oldTau

def isVisited(x, list):
    for i in range(len(list)):
        if list[i] == x:
            return True
    return False
def probability(Ant, TabuList):
    p = []
    # j = TabuList[len(TabuList) - 1]
    i = TabuList[len(TabuList) - 1]
    eps = 0.0000000001
    sum=0
    for k in range(Ant.len()):
        if not isVisited(k, TabuList):
            pheromoneLeftState = Ant.pheromone[i, k] ** alpha
            distanceLeftState = (1/(Ant.distance[i, k] + eps)) ** beta
            sum += (pheromoneLeftState * distanceLeftState)

    for j in range(Ant.len()):
        pheromoneNextState = Ant.pheromone[i, j] ** alpha
        distanceNextState = (1/(Ant.distance[i, j] + eps)) ** beta
        if not isVisited(j, TabuList):
            p.append((pheromoneNextState * distanceNextState) / sum)
        else:
            p.append(0)

    return np.argmax(p)

def calculateFitness(Ant,TabuList):
    fitness = 0
    for i in range(1,len(TabuList)):
        fitness += Ant.distance[TabuList[i]][TabuList[i-1]]

    return fitness

if __name__=='__main__':

    while(NC <= NCmax):
        fitness = []
        Ants = []
        AntTabu = []

        # 2. put down Ants to every single Node
        #    and set first node as a starting point
        for k in range(numOfAnt):
            Ants.append(rand.randint(0, X.len()-1))
            AntTabu.append([Ants[k]])

        # 3. visit every single node that not included in AntTabu
        s = 0
        while (s < X.len() - 1):
            s += 1
            for k in range(numOfAnt):
                # choose next state as random
                # count probability
                nextState = probability(X, AntTabu[k])
                AntTabu[k].append(nextState)
                if s == X.len() - 1:
                    AntTabu[k].append(AntTabu[k][0])
                    fitness.append(Fitness(route=AntTabu[k],
                                           fit=calculateFitness(X, AntTabu[k])))
        # print('Generation ', NC, ' >>', fitness[0].route, fitness[0].fit)
        fitness.sort(key=lambda fitness: fitness.fit)
        for Ant in range(numOfAnt):
            if best == []:
                best = fitness
            else:
                i = 0
                for ant in range(len(fitness)):
                    while (i < numOfAnt):
                        if fitness[ant].fit < best[i].fit:
                            best[i].route = fitness[ant].route
                            best[i].fit = fitness[ant].fit
                            break
                        i += 1
                    if i == numOfAnt:
                        break

        # 4. update pheromone
        pheromone = np.zeros([X.len(), X.len()])
        for k in range(len(best)):
            for i in range(1, len(best[k].route)):
                # increase pheromone
                pheromone[best[k].route[i], [best[k].route[i - 1]]] = deltaTau(
                    pheromone[best[k].route[i], [best[k].route[i - 1]]],
                    X.distance[best[k].route[i], [best[k].route[i - 1]]])

        for k in range(len(best)):
            for i in range(1, len(best[k].route)):
                # set evaporation
                X.pheromone = rho*X.pheromone + pheromone

        print('Gen-{} route:{} cost:{}'.format(NC,np.array(best[0].route)+1,best[0].fit))
        # print('cost',calculateFitness(X,best[0].route))
        NC += 1
        del fitness
    # print("\n --- Execution time: %s seconds ---" % (time.time() - start_time))
    X.test(best[0].route)
